ifeq ($(EMBUNIT_DIR),)
        EMBUNIT_DIR = $(subst \,/,/c/compiler/embunit)
endif

UNIT_SRC = $(wildcard $(EMBUNIT_DIR)/embUnit/*.c)

UNIT_OBJ = $(notdir $(EMBUNIT_SRC:.c=.obj))

UNIT_CFLAGS = -I$(EMBUNIT_DIR)